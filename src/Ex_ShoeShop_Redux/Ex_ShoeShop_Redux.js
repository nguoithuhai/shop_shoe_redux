import React, { Component } from "react";
import CartShoe from "./CartShoe";
import { shoeArr } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop_Redux extends Component {
  state = {
    shoeArr: shoeArr,
    detailShoe: shoeArr[0],
    cart: [],
  };
  handleChangeDetail = (shoe) => {
    this.setState({ detailShoe: shoe });
  };

  handleDelete = (id) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((shoe) => {
      return shoe.id == id;
    });
    cloneCart.splice(index, 1);
    this.setState({ cart: cloneCart });
  };
  handleChangeQuantity = (id, luaChon) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((shoe) => {
      return shoe.id == id;
    });
    cloneCart[index].soLuong = cloneCart[index].soLuong + luaChon;
    cloneCart[index].soLuong == 0 && cloneCart.splice(index, 1);
    // ** cloneCart[index].soLuong => true thì chạy splice, false thì dừng
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-7">
            <CartShoe
              handleChangeQuantity={this.handleChangeQuantity}
              handleDelete={this.handleDelete}
              cart={this.state.cart}
            />
          </div>
          <div className="col-5">
            <ListShoe />
          </div>
        </div>
        <DetailShoe handleChangeDetail={this.handleChangeDetail} />
      </div>
    );
  }
}

// truthy falsy

// 6 giá trị falsy: "",0,NaN,false,null,undefined
